package com.automation.qa;

import java.util.Random;

public class Lesson2 {

    public static void main(String[] args) {

        int myValue = 7;

        Random random = new Random();

        boolean isEqual = false;

        while(!isEqual) {
            int randomValue = random.nextInt(10);
            if (myValue == randomValue){
                System.out.println("Congratulation, the number was: " + myValue);
                isEqual = true;
            } else {
                System.out.println(randomValue);
            }
        }
    }
}
